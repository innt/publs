(defproject parser "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [com.taoensso/timbre "4.1.4"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/data.zip "0.1.1"]
                 [org.apache.commons/commons-compress "1.10"]
                 ;[clj-time "0.11.0"]
                 [org.clojure/java.jdbc "0.4.2"]
                 [org.postgresql/postgresql "9.4-1204-jdbc42"]]
  :main ^:skip-aot parser.core
  :target-path "target/%s"
  :profiles {:uberjar    {:aot :all}
             :dev        {:jvm-opts ["-Dlogfile.path=development"]}
             :test       {:jvm-opts ["-Dlogfile.path=test"]}
             :production {:jvm-opts ["-Dlogfile.path=production"]}})

(ns parser.parsers
    (:require [parser.db :as db]
      [taoensso.timbre :as log]
      [clojure.java.jdbc :as sql]
      [clojure.zip :as c-zip :refer [xml-zip]]
      [clojure.data.zip.xml :as c-d-z-xml :refer [xml-> xml1-> attr attr= text]]
      [clojure.pprint :refer [pprint]])
    (:import java.sql.SQLException))

(defmulti process :tag)

(def cnt (atom 0))

(defmethod process :article2
           [tag]
           ;(if (= "Editorial." (xml1-> (xml-zip tag) :title text))
           (pprint tag)
           ;)
           )

(defmethod process :article
           [article]
           (try
             (let [z (xml-zip article)
                   title (xml1-> z :title text)
                   journal (xml1-> z :journal text)
                   year (xml1-> z :year text)
                   url (xml1-> z :url text)
                   author (xml1-> z :author text)
                   journal-id (:id (db/journal-by-name journal))
                   author-id (:id (db/author-by-name author))
                   _ (sql/db-do-prepared db/db-spec "insert into publication(title, year, url, container_id) values(?, ?, ?, ?)"
                                         [title (read-string year) url journal-id])
                   pub_id (-> (sql/query db/db-spec
                                         ["select id from publication where title=? and container_id=?" title journal-id])
                              first :id)]
                  (sql/db-do-prepared db/db-spec "insert into publication_author(publication_id, person_id) values(?, ?)"
                                      [pub_id author-id])

                  )
             (catch Exception e (str e))
             (finally (swap! cnt inc)
                      (when (= 0 (mod @cnt 1000)) (log/info (str "parsed " @cnt))))))

(defmethod process :inproceedings [tag])
(defmethod process :proceedings [tag])
(defmethod process :book [tag])
(defmethod process :incollection [tag])
(defmethod process :phdthesis [tag])
(defmethod process :mastersthesis [tag])
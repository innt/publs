(ns parser.db
  (:require [clojure.java.jdbc :as sql]))

(def db-spec
  "postgresql://localhost:5432/publs?user=krikun&password=Iam13!01")

(def journal-type-id
  (-> (sql/query db-spec ["select id from container_type where name=?" "journal"]) first :id))

(def journal-by-name
  (fn [name]
    (if-let [id (-> (sql/query db-spec ["select c.* from container c
      join container_type ct on ct.id = c.type_id
      where ct.id=? and c.title=?" journal-type-id name]) first)]
      id
      (do
        (sql/db-do-prepared db-spec "insert into container(type_id, title) values(?,?)"
                            [journal-type-id name])
        (recur name)))))

(def author-by-name
  (fn [name]
    (let [[fname _sname] (clojure.string/split name #" ")
          sname (or _sname "")]
      (if-let [id (-> (sql/query db-spec ["select * from person where first_name=? and second_name=?" fname sname]) first)]
        id
        (do
          (sql/db-do-prepared db-spec "insert into person(first_name, second_name) values(?,?)"
                              [fname sname])
          (recur name))))))
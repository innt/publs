(ns parser.core
  (:gen-class)
  (:require
    ;[clojure.tools.logging :as log]
    [parser.parsers :as parsers]
    [taoensso.timbre :as log]
    [clojure.xml :as c-xml]
    [clojure.data.xml :as c-d-xml :refer [parse]]
    [clojure.zip :as c-zip :refer [xml-zip]]
    [clojure.data.zip :as c-d-zip]
    [clojure.data.zip.xml :as c-d-z-xml :refer [xml-> xml1-> attr attr= text]]
    [clojure.java.io :as io]
    ;[clj-time.core :as time]
    ;[clj-time.format :as fmt]
    [clojure.pprint :refer [pprint]]
    [clojure.java.jdbc :as sql])
  (:import [org.apache.commons.compress.compressors.gzip
            GzipCompressorInputStream]))

(defn gzip-reader
  "produce a Reader on a gzipped file"
  [filename]
  (-> filename
      io/file
      io/input-stream
      GzipCompressorInputStream.
      io/reader))


(defn article->map
  [article]
  (let [z (xml-zip article)]
    {:title (xml1-> z :title text)}
    ))


(defn parse-dblp [filename]
  (with-open [rdr (gzip-reader filename)]
    (doseq [tag (take 1300000
                      (->> rdr
                           parse
                           :content
                           (filter #(contains? (set [:article]) (:tag %)))))]
      ; :inproceedings :proceedings :book :incollection :phdthesis :mastersthesis
      (parsers/process tag))))


(defn -main [& args]
  (log/info "starting job")
  (time (parse-dblp "data/dblp.xml.gz"))
  (log/info "job's done")
  (shutdown-agents))


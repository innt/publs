(ns web.utils.masonry)

(defn masonry []
  (prn "masonry!!!")
  (def params (js-obj "percentPosition" true "itemSelector" ".masonry-grid > .col"))
  (def element (.getElementById js/document "grid"))
  (def msnry (js/Masonry. element params))
  (.layout msnry))

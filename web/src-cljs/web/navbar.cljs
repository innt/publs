(ns web.navbar
  (:require
    [reagent.session :as session]
    [ajax.core :refer [GET POST]]
    [web.components.fl-input :as fl-input])
  (:import goog.History))

(defn nav-link [uri title page open?]
  [:li {:class (when (= page (session/get :page)) "active")}
   [:a {:href     uri
        :on-click #(reset! open? not)}
    title]])

(defn navbar []
  (let [open? (atom false)]
    (fn []
      [:nav.navbar.inverse
       [:div.nav-wrapper
        [:a.brand-logo {:href "#/"}
         [:i.i.i-publs.left {:style {:margin-left ".5em"}}]
         "PUBLS"]

        [:ul.right.hide-on-med-and-down
         [:li
          [:div.input-field
           [fl-input/input {:label [:i.material-icons "search"] :get-records true}]
           ;[:label {:for "search"} [:i.material-icons "search"]]
           ]
          ]
         [:li
          [:a.university-logo {:href "http://university.innopolis.ru/"}
           [:img {:src "svg/logo.svg" :alt "Innopolis University" :style {:height "48px"}}]]]
         [nav-link "#/" "Home" :home open?]
         [nav-link "#/about" "About" :about open?]
         [nav-link "#/edit" "Edit" :edit open?]
         [nav-link "#/records" "Records" :records open?]]

        ;[:ul#nav-mobile.side-nav
        ; (when @open? {:class "open"})
        ; [nav-link "#/" "Home" :home open?]
        ; [nav-link "#/about" "About" :about open?]]

        ;[:a.button-collapse {:href "#"
        ;                     :on-click #(swap! open? not)}
        ; [:i.material-icons "menu"]]
        ]])))


(ns web.pages.home
  (:require [reagent.core :as reagent]))

(defn home-page []
  (let [open? (reagent/atom false)]
    (fn []
      [:div.container>div.row>div.col.s1.m2-3.offset-m6.l2.offset-l4>div.card.z-depth-2
       [:div.card-content
        [:div.card-title.grey-text.text-darken-4
         {:on-click #(swap! open? not)}
         "Log in"
         [:i.material-icons "more_vert"]]
        [:div.container
         [:div.row>div.input-field.col.s1
          [:div.addon [:i.material-icons.prefix "account_circle"]
           [:input#email.validate {:type "email" :placeholder "Email"}]]]
         [:div.row>div.input-field.col.s1
          [:div.addon [:i.material-icons.prefix "vpn_key"]
           [:input#password.validate {:type "password" :placeholder "Password"}]]]
         [:button.btn.btn-block.waves-effect.waves-block.waves-light.green "Log in"]]]
       [:div.card-reveal {:class (if @open? "open" "close")}
        [:span.card-title.grey-text.text-darken-4 {:on-click #(swap! open? not)}
         "Registration"
         [:i.material-icons "close"]]
        [:div.container
         [:p "To use this system you need account."]
         [:button.btn.btn-block.waves-effect.waves-block.waves-light "Registration"]
         [:p "What can I do if " [:a {:href "#"} "forgot password?"]]]]])))
(ns web.pages.records
  (:require
    [web.components.record :as record-component]
    [reagent.core :as reagent]
    [ajax.core :refer [GET POST]]
    [web.utils.masonry :refer [masonry]]
    [cljs.reader :refer [read-string]]))

(def records-list (reagent/atom nil))

(defn handler [response]
  ;(.log js/console (str "RESPONSE: " response))
  ;(prn response)
  (reset! records-list response))


(defn error-handler [{:keys [status status-text]}]
  (.log js/console
        (str "something bad happened: " status " " status-text)))

(defn get-records [val]
  (GET "/api/publications" {:handler         handler
                            :response-format :json
                            :keywords?       true
                            :params          {:limit  20
                                              :offset 1
                                              :title  val}}))


(defn records-menu
  "return side menu structure"
  []
  [:div.col.s1.m3.l5
   [:div.side-menu
    [:div.card-embeded
     [:p.grey-text.text-lighten-1 "BROWSE"]
     [:div.card-collection
      [:a.item.active {:href "#!"}
       [:i.material-icons "assessment"]
       "Top Publs"]
      [:a.item {:href "#!"}
       [:i.material-icons "recent_actors"]
       "Authors"]
      [:a.item {:href "#!"}
       [:i.material-icons "perm_media"]
       "Category"]]
     [:p.grey-text.text-lighten-1
      "YOUR PUBLS"
      [:a.right.success {:href "#/edit"}
       [:i.material-icons "add"]
       ]]
     [:div.card-collection
      [:a.item {:href "#!"}
       [:i.material-icons "subject"]
       "Drafts"]
      [:a.item {:href "#!"}
       [:i.material-icons "class"]
       "Published"]]
     [:p.grey-text.text-lighten-1
      "SHELVES"
      [:a.right.success {:href "#"}
       [:i.material-icons "add"]]]
     [:div.card-collection
      [:a.item {:href "#!"}
       [:i.material-icons "class"]
       "DMD"]
      [:a.item {:href "#!"}
       [:i.material-icons "assessment"]
       "DSA"]
      [:a.item {:href "#!"}
       [:i.material-icons "assessment"]
       "OOP"]]]]]
  )

(defn grid-menu
  []
  [:div.col.s1.m2-3.l4-5
   [:div.hide-on-med-and-down
    [:div.card
     [:nav
      [:div.nav-wrapper
       [:ul.left
        [:li [:a {:href "#"} "Top publication"
              [:i.right.material-icons "chevron_right"]]]

        [:li [:a {:href "#"} "Literature"
              [:i.right.material-icons "chevron_right"]]]

        [:li.active [:a {:href "#"} "All time"
                     [:i.right.material-icons "chevron_right"]]]
        ]
       [:ul#nav-mobile.right.hide-on-med-and-down
        [:li.active {:on-click #(swap! records-list (partial sort-by :title))}
         [:a
          [:i.right.material-icons "swap_vert"]
          "Name"]]

        [:li [:a {:href "#"} "Date"]]
        [:li [:a {:href "#"} "Others"]]
        [:li [:a {:href "#"}
              [:i.material-icons "view_list"]]]]]]]]]
  )

(defn records
  "return records elements"
  []
  (fn []
    (into [:div#grid.masonry-grid.row]
          [
           ;(prn @records-list)
           (records-menu)
           (grid-menu)
           (map (fn [record] [record-component/record record ]) @records-list)
           ]))
  )

(defn records-page
  "fn return card app component"
  []
  (reagent/create-class
    {:component-did-mount  #(masonry)
     :component-did-update #(masonry)
     :reagent-render       #(records)
     }))


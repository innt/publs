(ns web.pages.edit
  (:require [reagent.core :as reagent]
            [web.components.edit-record :as edit-record]
            [web.utils.masonry :refer [masonry]]
            [ajax.core :refer [GET POST]]
            [cljs.reader :refer [read-string]]
            [web.components.record :as record-component]))

(def draft-list (reagent/atom nil))

(defn handler [response]
  ;(.log js/console (str "RESPONSE: " response))
  (swap! draft-list (fn [] (-> response read-string))))

(defn error-handler [{:keys [status status-text]}]
  (.log js/console
        (str "something bad happened: " status " " status-text)))

(defn get-drafts []
  (GET "/drafts" {:handler handler}))


(defn edit-menu
  "return side menu structure"
  []
  [:div.col.s1.m3.l5
   [:div.side-menu
    [:div.card-embeded
     [:p.grey-text.text-lighten-1 "ADD"]
     [:div.card-collection
      [:a.item.active {:href "#!"}
       [:i.material-icons "assessment"]
       "Publication"]
      [:a.item {:href "#!"}
       [:i.material-icons "class"]
       "Container"]]
     [:p.grey-text.text-lighten-1
      "YOUR PUBLS"
      ;[:a.right.success {:href "#/edit"}
      ; [:i.material-icons "add"]
      ; ]
      ]
     [:div.card-collection
      [:a.item {:href "#!"}
       [:i.material-icons "subject"]
       "Drafts"]
      [:a.item {:href "#!"}
       [:i.material-icons "class"]
       "Published"]]
     [:p.grey-text.text-lighten-1
      "SHELVES"
      [:a.right.success {:href "#"}
       [:i.material-icons "add"]]]
     [:div.card-collection
      [:a.item {:href "#!"}
       [:i.material-icons "class"]
       "DMD"]
      [:a.item {:href "#!"}
       [:i.material-icons "assessment"]
       "DSA"]
      [:a.item {:href "#!"}
       [:i.material-icons "assessment"]
       "OOP"]]]]]
  )
;
;(defn edit-page []
;  (let [open? (reagent/atom false)]
;    (fn []
;      [:div#grid.masonry-grid.row
;       (edit-menu)
;       [edit-record/edit nil]
;       ])))

(defn edit-page
  "return edit page component"
  []
  (get-drafts)
  (fn []
    (into [:div#grid.masonry-grid.row]
          [
           ;(prn @records-list)
           (edit-menu)
           (edit-record/edit nil)
           (map (fn [record] [record-component/record record]) @draft-list)
           ]))
  )

(defn records-page
  "fn return card app component"
  []
  (reagent/create-class
    {:component-did-mount #(masonry)
     :component-did-update #(masonry)
     :reagent-render      #(edit-page)
     }))
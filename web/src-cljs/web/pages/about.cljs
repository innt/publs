(ns web.pages.about
  (:require
    [markdown.core :refer [md->html]]
    [reagent.session :as session]
    [ajax.core :refer [GET POST]]
    ))

(defn about-page []
  [:div.container
   [:h2 "Welcome to PUBLS!"]
   [:p "System build with lumius framework."]
   [:p [:a.btn.btn-primary.btn-lg {:href "http://luminusweb.net"} "Learn more »"]]
  (when-let [docs (session/get :docs)]
    [:div
     [:div
      [:div {:dangerouslySetInnerHTML
             {:__html (md->html docs)}}]]])])

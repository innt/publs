(ns web.core
  (:require
            [web.navbar :as navbar]
            [web.pages.about :as about-page]
            [web.pages.home :as home-page]
            [web.pages.edit :as edit-page]
            [web.pages.records :as records-page]
            [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [secretary.core :as secretary :include-macros true]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [ajax.core :refer [GET POST]])
  (:import goog.History))

(def pages
  {:home #'home-page/home-page
   :records #'records-page/records-page
   :edit #'edit-page/edit-page
   :about #'about-page/about-page
   })

(defn fetch-docs! []
  (GET (str js/context "/docs") {:handler #(session/put! :docs %)}))

(defn page []
  [(pages (session/get :page))])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
  (session/put! :page :home))

(secretary/defroute "/about" []
  (session/put! :page :about))

(secretary/defroute "/edit" []
  (session/put! :page :edit))

(secretary/defroute "/records" []
  (session/put! :page :records))

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
        (events/listen
          EventType/NAVIGATE
          (fn [event]
              (secretary/dispatch! (.-token event))))
        (.setEnabled true)))

;; -------------------------
;; Initialize app

(defn mount-components []
  (reagent/render [#'navbar/navbar] (.getElementById js/document "navbar"))
  (reagent/render [#'page] (.getElementById js/document "app")))

(defn init! []
  (fetch-docs!)
  (hook-browser-navigation!)
  (mount-components))

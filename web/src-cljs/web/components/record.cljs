(ns web.components.record
  (:require [reagent.core :as r]))

(defn record
  [record]
  (prn record)
  (let [open? (r/atom false)]
    (fn []
      [:div.col.s1.m3.l5
       [:div.card
        [:div.card-content
         (if (some? (:img record))
           [:div.card-image
            [:img.activator {:src (:img record)}]])
         [:div.card-title.grey-text.text-darken-4
          {:on-click #(swap! open? not)}
          (:title record)
          [:i.material-icons "more_vert"]]
         (if (some? (:container record))
           [:div.card-container
            [:div.grey-text.light "Container:"]
            (:container record)])
         (if (some? (:author record))
           [:div.card-author
            [:div.grey-text.light "Authors:"]
            (if-let [fname (get-in record [:author :first-name])] fname)
            " "
            (if-let [sname (get-in record [:author :second-name])] sname)])
         (if (some? (:year record))
           [:div.card-year.grey-text.text-lighten-2.right-align>strong (:year record)])
         ]
        [:div.card-action
         [:a {:href (str "http://dblp.uni-trier.de/" (:url record))} "LINK"]
         [:a.right.grey-text {:href "#"} "to shelve"]
         ]
        [:div.card-reveal {:class (if @open? "open" "close")}
         [:span.card-title.grey-text.text-darken-4 {:on-click #(swap! open? not)}
          (:title record)
          [:i.material-icons "close"]]
         [:div.views-count
          [:em.grey-text "View count: "]
          [:strong.right (if (some? (:views-count record)) (:views-count record) 0)]
          ]
         [:div.reviews
          [:em.grey-text "Reviews: "]
          [:strong.right (if (some? (:reviews record)) (:reviews record) 0)]
          ]
         [:button.btn.waves-effect.waves-block.waves-light.red.darken-3.right
          {:style {:position "absolute"
                   :bottom "1rem"
                   :right "1rem"}}
          "delete"]
         ]
        ]])))

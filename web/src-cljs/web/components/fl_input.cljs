(ns web.components.fl-input
  (:require [reagent.core :as r]
            [secretary.core :as secretary :include-macros true]
            [reagent.session :as session]))


(defn input
  "float label input"
  [{:keys [name value label get-records]}]
  (let [cur-val (r/atom value)]
    (fn [props]
      (let [down? (empty? @cur-val)]
        [:div
         [:input (merge props
                        {:type        "text"
                         :value       @cur-val
                         ;:on-blur #(web.pages.records/get-records)
                         :on-change   #(reset! cur-val (-> % .-target .-value))
                         :on-key-down #(when get-records
                                        (case (.-which %)
                                          13 (do
                                               (web.pages.records/get-records @cur-val)
                                               ;(session/put! :page :records)
                                               (secretary/dispatch! "records")
                                               (set! (.-hash js/location) "/records")
                                          ;27 (stop)
                                          nil)))})]
         [:label {:for   (:id props)
                  :class (if-not down? "active" "")
                  } label]
         ]
        ))))
(ns web.components.card
  (:require [reagent.core :as r]))

(defn card
  [card]
  (let [open? (r/atom false)]
    (fn []
      [:div.col.s1.m3.l5
       [:div.card
        [:div.card-content
         (if (some? (:img card))
           [:div.card-image
            [:img.activator {:src (:img card)}]])
         [:div.card-title.activator.grey-text.text-darken-4
          {:on-click #(swap! open? not)}
          [:i.material-icons.right "more_vert"]
          (:title card)]
         (if (some? (:container card))
           [:div.card-container (:container card)])
         (if (some? (:author card))
           [:div.card-author
            (if-let [fname (get-in card [:author :first-name])] fname)
            " "
            (if-let [sname (get-in card [:author :second-name])] sname)])
         (if (some? (:year card))
           [:div.card-year (:year card)])
         ]
        [:div.card-reveal {:class (if @open? "open" "close")}
         [:span.card-title.grey-text.text-darken-4 {:on-click #(swap! open? not)}
          [:i.material-icons.right "close"]
          (:title card)]
         ]
        ]])))

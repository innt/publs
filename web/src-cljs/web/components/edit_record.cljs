(ns web.components.edit-record
  (:require
    [web.components.fl-input :as fl-input]
    [web.components.fl-textarea :as fl-textarea]
    ))

(defn edit
  [record]

  [:div.col.s1.m2-3.l4-5
   [:div.card
    [:div.card-content
     [:form {:action ""}
      [:div.row>div.input-field>div.with-addons
       [:input.title {:type "text" :placeholder "Title"}]
       [:i.material-icons "more_vert"]]
      [:div.row.multiply
       [:div.input-field.col.s1.l2
        [:div.with-addons
         [:i.material-icons "perm_identity"]
         [fl-input/input {:id "first_name" :label "First Name"}]
         [fl-input/input {:id "second_name" :label "Second Name"}]
         [:i.material-icons.success-text "add"]
         ]
        ]
       [:div.input-field.col.s1.l2 {:style {:paddingLeft "2rem"}}
        [:div.with-addons
         [:i.material-icons "link"]
         [fl-input/input {:id "link" :label "URL"}]
         [:i.material-icons.success-text "add"]
         ]
        ]
       ]
      ]
     [:div.row.cf
      [:div.input-field.col.s1
       [fl-textarea/textarea {:id "abstract" :label "Abstract"}]
       ]
      ]
     [:div.row.cf
      [:div.input-field.col.s1.l4
       [:div.with-addons
        [:i.material-icons "perm_media"]
        [fl-input/input {:id "category" :label "Category"}]
        [:i.material-icons.success-text "add"]
        ]
       ]
      [:div.input-field.col.s1.l3-4
       [:div.with-addons
        [:i.material-icons "label"]
        [fl-input/input {:id "tags" :label "Tags"}]
        ]
       ]
      ]
     ]
    [:div.card-action
     [:a {:href "#"} "save in drafts"]
     [:button.btn.waves-effect.waves-block.waves-light.green.right "add"]
     ]
    ]]
  ;<div class="card-action">
  ;<a href="#">Save in Drafts</a>
  ;<button class="btn waves-effect waves-block waves-light green right">Add</button>
  ;</div>

  ;<div class="card-reveal">
  ;<span class="card-title grey-text text-darken-4">Registration<i class="material-icons right">close</i></span>
  ;<p>To use this system you need account.</p>
  ;<button class="btn btn-block waves-effect waves-block waves-light">Registration</button>
  ;<p>
  ;What can I do if
  ;<a href="#">forgot password?</a>
  ;</p>
  ;</div>
  ;</div>
  ;<!-- </div> -->
  ;</div>

  )
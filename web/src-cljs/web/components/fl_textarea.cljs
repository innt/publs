(ns web.components.fl-textarea
  (:require [reagent.core :as r]))

(defn textarea
  "float label input"
  [{:keys [value label count]}]
  (let [val (r/atom value)
        height (r/atom 43)]
    (fn [props]
      (let [down? (empty? @val)]
        [:div
         [:textarea.materialize-textarea
          (merge props
                 {:style     {:height @height}
                  :on-change #(reset! val (-> % .-target .-value))
                  :on-key-up #(reset! height (+ (-> % .-target .-parentNode .-lastChild .-clientHeight) 20))
                  }) @val]
         [:label {:for   (:id props)
                  :class (if-not down? "active" "")
                  } label]
         ;textarea-sizer must be last child
         [:div.textarea-sizer (str @val "lastline")]
         ]
        ))))
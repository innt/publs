CREATE TABLE IF NOT EXISTS container (
  id           SERIAL PRIMARY KEY,
  title        VARCHAR(255),
  abbr         VARCHAR(20),
  number       VARCHAR(100),
  edition      VARCHAR(50),
  date         DATE,
  url          VARCHAR(255),
  pages        VARCHAR(50),
  volume       VARCHAR(50),
  parent_id    INTEGER REFERENCES container (id) ON DELETE SET NULL,
  publisher_id INTEGER REFERENCES publisher (id) ON DELETE CASCADE,
  type_id      INTEGER REFERENCES container_type (id) ON DELETE CASCADE,
  series_id    INTEGER REFERENCES series (id) ON DELETE SET NULL,
  image_id     INTEGER REFERENCES image (id) ON DELETE SET NULL,
  UNIQUE (title, type_id)
);
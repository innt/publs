CREATE TABLE IF NOT EXISTS person_role (
  person_id INTEGER REFERENCES person (id) ON DELETE CASCADE,
  role_id   INTEGER REFERENCES role (id) ON DELETE CASCADE,
  PRIMARY KEY (person_id, role_id)
);

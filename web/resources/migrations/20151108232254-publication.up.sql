CREATE TABLE IF NOT EXISTS publication (
  id           SERIAL PRIMARY KEY,
  title        VARCHAR(255),
  abstract     TEXT,
  year         INTEGER,
  url          VARCHAR(255),
  view_count   INTEGER,
  container_id INTEGER REFERENCES container (id) ON DELETE CASCADE,
  UNIQUE (title, container_id)
);
CREATE INDEX ON publication (container_id, title);
CREATE INDEX ON container (title);
CREATE INDEX ON person (first_name, second_name);
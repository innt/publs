CREATE TABLE IF NOT EXISTS publication_references (
  publication_from_id INTEGER REFERENCES publication (id) ON DELETE CASCADE,
  publication_to_id   INTEGER REFERENCES publication (id) ON DELETE CASCADE,
  PRIMARY KEY (publication_from_id, publication_to_id)
);

CREATE TABLE IF NOT EXISTS role_permission (
  role_id       INTEGER REFERENCES role (id),
  permission_id INTEGER REFERENCES permission (id)
);
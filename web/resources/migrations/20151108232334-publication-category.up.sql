CREATE TABLE IF NOT EXISTS publication_category (
  publication_id INTEGER REFERENCES publication (id) ON DELETE CASCADE,
  category_id    INTEGER REFERENCES category ON DELETE CASCADE,
  PRIMARY KEY (publication_id, category_id)
);
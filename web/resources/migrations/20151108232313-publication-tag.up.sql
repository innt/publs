CREATE TABLE IF NOT EXISTS publication_tag (
  publication_id INTEGER REFERENCES publication (id) ON DELETE CASCADE,
  tag_id         INTEGER REFERENCES tag (id) ON DELETE CASCADE,
  PRIMARY KEY (publication_id, tag_id)
);

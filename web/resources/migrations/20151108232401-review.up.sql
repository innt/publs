CREATE TABLE IF NOT EXISTS review (
  id             SERIAL PRIMARY KEY,
  text           TEXT,
  mark           INTEGER,
  publication_id INTEGER REFERENCES publication(id) on DELETE CASCADE,
  person_id      INTEGER REFERENCES person(id) ON DELETE CASCADE
);
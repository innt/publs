CREATE TABLE IF NOT EXISTS publication_author (
  publication_id INTEGER REFERENCES publication (id) ON DELETE CASCADE,
  person_id      INTEGER REFERENCES person (id),
  PRIMARY KEY (publication_id, person_id)
);
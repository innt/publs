CREATE TABLE IF NOT EXISTS publication_adviser (
  publication_id INTEGER REFERENCES publication (id) ON DELETE CASCADE,
  person_id      INTEGER REFERENCES person (id) ON DELETE CASCADE,
  PRIMARY KEY (publication_id, person_id)
);
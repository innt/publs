CREATE TABLE IF NOT EXISTS person (
  id          SERIAL PRIMARY KEY,
  first_name  VARCHAR(50),
  second_name VARCHAR(100),
  birthday    DATE,
  grade       VARCHAR(200),
  info        VARCHAR(255),
  email       VARCHAR(100),
  password    VARCHAR(255)
);
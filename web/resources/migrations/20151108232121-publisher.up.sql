CREATE TABLE IF NOT EXISTS publisher (
  id          SERIAL PRIMARY KEY,
  name        VARCHAR(100),
  address     VARCHAR(255),
  url         VARCHAR(255),
  description TEXT
);
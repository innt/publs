CREATE TABLE IF NOT EXISTS container_editor (
  container_id INTEGER REFERENCES container (id) ON DELETE CASCADE,
  person_id    INTEGER REFERENCES person (id) ON DELETE CASCADE,
  PRIMARY KEY (container_id, person_id)
);

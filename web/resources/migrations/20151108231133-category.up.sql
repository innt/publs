CREATE TABLE IF NOT EXISTS category (
  id        SERIAL PRIMARY KEY,
  name      VARCHAR(100),
  parent_id INTEGER REFERENCES category (id) ON DELETE SET NULL
);
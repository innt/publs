(ns web.routes.home
  (:require [web.layout :as layout]
            [compojure.core :refer [defroutes GET]]
            [ring.util.http-response :refer [ok]]
            [clojure.java.io :as io]))

;(defonce records-list )
(def records-list [{:id        1
                    :title     "It works! 1 But sometimes shit happens"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        2
                    :title     "It works either! 2"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        3
                    :title     "It works either 3!"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        4
                    :title     "It works either! Thourth 4"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        5
                    :title     "It works either! Fifth 5"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        6
                    :title     "It works either! 6"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        7
                    :title     "It works! 7"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        8
                    :title     "It works either! 8"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        9
                    :title     "It works either! Third 9"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        10
                    :title     "It works either! Thourth 10"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        11
                    :title     "It works either! Fifth 11"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        12
                    :title     "It works either! 12"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        13
                    :title     "It works! 13"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        14
                    :title     "It works either! 14"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        15
                    :title     "It works either! Third 15"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        16
                    :title     "It works either! Thourth 16"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        17
                    :title     "It works either! Fifth 17"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        18
                    :title     "It works either! 18"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   ])

(def draft-list [{:id        21
                    :title     "Draft But sometimes shit happens"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        22
                    :title     "Draft 2"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        23
                    :title     "Draft !"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        24
                    :title     "Draft Thourth 4"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        25
                    :title     "Draft Fifth 5"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        26
                    :title     "Draft 6"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        27
                    :title     "Draft "
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   {:id        28
                    :title     "Draft 8"
                    :year      "1998"
                    :container "Simple Questions about that"
                    :author    {:first-name "ExampleFirstName" :second-name "ExampleSecondName"}}
                   ])

(defn home-page []
  (layout/render "home.html"))

(defn records []
  (prn-str records-list))

(defroutes home-routes
           (GET "/" [] (home-page))
           (GET "/docs" [] (ok (-> "docs/docs.md" io/resource slurp)))
           (GET "/echo" request (str request))
           (GET "/records" [] (records))
           (GET "/drafts" [] (prn-str draft-list)))


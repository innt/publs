# web

FIXME

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

profiles.clj
{:profiles/dev  {:env {:database-url "jdbc:postgresql://localhost/publs"}}
 :profiles/test {:env {:database-url "jdbc:postgresql://localhost/web_test?user=db_user_name_here&password=db_user_password_here"}}}

To start a web server for the application, run:

    lein run

## License

Copyright © 2015 FIXME

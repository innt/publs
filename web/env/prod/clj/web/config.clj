(ns web.config
  (:require [taoensso.timbre :as timbre]))

(def defaults
  {:init
   (fn []
     (timbre/info "\n-=[web started successfully]=-"))
   :middleware identity})

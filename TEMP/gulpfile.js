var gulp = require('gulp');
var connect = require('gulp-connect');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');

var fontName = 'Icons';

gulp.task('iconfont', function() {
  gulp.src(['assets/icons/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
      path: 'assets/templates/icons-template.css',
      targetPath: '../../css/icons.css',
      fontPath: '../font/icons/',
      cssClass: 'i'
    }))
    .pipe(iconfont({
      appendUnicode: true,
      formats: ['ttf', 'eot', 'woff', 'svg'],
      fontName: fontName,
      normalize: true,
      fontHeight: 40
     }))
    .pipe(gulp.dest('static/font/icons/'));
});

gulp.task('server-connect', function () {
  connect.server({
    root: 'static',
    port: 80,
    // livereload: true
  });
});

gulp.task('move-js', function() {
    gulp.src(['node_modules/materialize-css/dist/js/*.js'])
      .pipe(gulp.dest('static/js/'));
});

gulp.task('move', function() {
    gulp.src(['node_modules/materialize-css/dist/css/*.css'])
      .pipe(gulp.dest('static/css/'));
});

gulp.task('html', function () {
  gulp.src('**/*.html')
    .pipe(connect.reload());
});

gulp.task('watch-dist', function() {
  gulp.watch(['node_modules/materialize-css/dist/css/*.css'], ['move']);
  gulp.watch(['node_modules/materialize-css/dist/js/*.js'], ['move-js']);
});

gulp.task('server',['watch-dist', 'server-connect']);

gulp.task('watch', function () {
  gulp.watch(['**/*.html'], ['html']);
  gulp.watch(['assets/icons/*.svg'], ['iconfont']);
  gulp.watch(['node_modules/materialize-css/dist/css/*.css'], ['move', 'html']);
  gulp.watch(['node_modules/materialize-css/dist/js/*.js'], ['move-js', 'html']);
});

gulp.task('default', ['iconfont', 'server-connect', 'watch']);
(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('select').material_select();

  }); // end of document ready
  var initMasonry = function() {
    $('.masonry-grid').masonry({
      itemSelector: '.masonry-grid > .col',
      percentPosition: true
    });
  };
  $(document).on('ready', function () {
    initMasonry();
    setTimeout(initMasonry, 200);
  });
})(jQuery); // end of jQuery name space
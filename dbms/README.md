# hw9

FIXME: description

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar hw9-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

## License

Copyright © 2015 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.

; find records in table
; - full scan,
;   filter
; - use nonclustered index -> [id id id]
;   use clustered index for each id
;   filter
; - use clustered index id


select * from students where
    (name > "nm" and salary > 2 )                           [id1 id2]
    or (name like '%stu' and salary between 12 and 40)      [id3 id1]
    or (name = "Hey" and name2 > 7 and salary = 8)          [id4 id2 id3]  | then :id [id1 id2 id3 id1 id4 id2 id3]
                                                                           | or then :id [id1 id2 id3 id4]

use index :name, filter > "nm"  -> [id id]
use index :id cluster [id id], filter salary > 2 -> [rec rec]

or (name = "Hey" and name2 > 7 and salary = 8)
use index :name, filter = "Hey" -> [id id id]
cannot use index :name2, filter > 7 [id2 id2]
use index :id [id id id], filter salary = 2 and name2 > 7 -> [rec rec]

use index :name2, filter > 7 [id2 id2]
use index :id [id2 id2], filter salary = 2 and name = "hey" -> [rec rec]
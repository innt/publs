(ns dbms.page.index
  (:require [dbms.record :as rec]
            [dbms.cache :as cache]
            [taoensso.timbre :as timbre]
            [dbms.page.core :as page]))

(defmethod rec/find-record :index
  [page key-value]
  (if-let [record (page/find-record-or-prev page key-value)]
    (let [next-page (cache/pull-page {:page-id (:page-id record)})
          path (into [] (:path (meta page)))
          next-page-with-path (with-meta next-page {:path (conj path next-page)})]
      (rec/find-record next-page-with-path key-value))))

(defn find-data-page
  "find page in wich record must be inserted"
  [page key-value]
  (if-let [record (page/find-record-or-prev page key-value)]
    (let [next-page (cache/pull-page {:page-id (:page-id record)})
          path (into [] (:path (meta page)))
          next-page-with-path (with-meta next-page {:path (conj path next-page)})]
      (if (= (:type next-page) :data)
        next-page-with-path
        (recur next-page-with-path key-value)))))

(defmethod rec/insert-record :index
  [page record]
  (let [key (:key (meta (:fields page)))
        key-value (key record)
        subnode-to-insert (page/find-record-or-prev page key-value)
        [page new-child-node child-pivot] (rec/insert-record subnode-to-insert record)]
    (if (nil? new-child-node)
      [page nil nil]
      (let [record {key child-pivot :page-id new-child-node}]
        (cache/push-page new-child-node)
        (if (page/has-space-for-record? page record)
          [(page/insert-record page record) nil nil]              ; [page new-leaf new-pivot]
          (page/split page record))))))

;(def data-page
;  (->> (page/new-page {:type :data :page-id 1})
;       (page/with-records (with-meta {:id :number :name :string} {:key :id}))
;       (page/insert-record {:id 1 :name "John"})
;       (page/insert-record {:id 3 :name "Smith"})
;       cache/push-page))
;
;(def data-page1
;  (->> (page/new-page {:type :data :page-id 2})
;       (page/with-records (with-meta {:id :number :name :string} {:key :id}))
;       (page/insert-record {:id 12 :name "John 1"})
;       (page/insert-record {:id 30 :name "Smith 1"})
;       cache/push-page))
;
;(def index-page
;  (->> (page/new-page {:type :index :page-id 0})
;       (page/with-records (with-meta {:id :number :page-id :number} {:key :id}))
;       (page/insert-record {:id 1 :page-id 1})
;       (page/insert-record {:id 10 :page-id 2})))
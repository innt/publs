(ns dbms.page.core
  (:require
    [dbms.record :as rec]
    [dbms.config :as config]
    [dbms.file :as f]
    [gloss core io]
    [dbms.core :as core]
    [taoensso.timbre :as timbre])
  (:import [java.nio ByteBuffer]))

(gloss.core/defcodec slots-frame (gloss.core/repeated (rec/type->codec :number) :prefix :none))

(def header-frame-map {:free-count  (rec/type->codec :number)
                       :free-offset (rec/type->codec :number)
                       :level       (rec/type->codec :number)
                       :next-page   (rec/type->codec :number)
                       :page-id     (rec/type->codec :number)
                       :prev-page   (rec/type->codec :number)
                       :slot-count  (rec/type->codec :number)
                       :type        (gloss.core.codecs/enum :byte {:data \d :index \i :boot \b})})
(gloss.core/defcodec header-frame header-frame-map)
(def header-frame-size (+ 1 (* rec/size-of-number (dec (count header-frame-map)))))

(defn free-space
  "how many bytes you can insert (for new record) (one slot index reserved)"
  [page]
  (- config/*page-size*
     header-frame-size
     (-> page :slot-count (* rec/size-of-number))))

(defn make-page
  [defaults]
  (let [buf (ByteBuffer/allocate config/*page-size*)]
    (conj {:dirty       false                               ; some fields chaned, need to update memory buffer
           :page-id     0
           :type        :data
           :level       0
           :next-page   -1
           :prev-page   -1
           :slot-count  0
           :free-count  (free-space {:slot-count 0})
           :free-offset header-frame-size
           :slots       []
           ;:records     []
           :raw         buf}
          defaults)))


(defn new-page
  "creates empty page, index is last in file"
  [& [defaults]]
  (-> (conj (make-page {:dirty   true
                        :page-id (f/page-count)})
            defaults)
      (f/write-page)))

(defn with-records
  [page fields]
  (assoc page :fields fields))


(defn pack-record
  [page fields rec offset]
  (let [raw (:raw page)
        bin (rec/pack-record fields rec)]
    (.position raw offset)
    (.put raw (byte-streams/to-byte-array bin))))


(defn pack-records
  [page fields]
  ;(doseq [[offset rec] (map list (:slots page) (:records page))]
  ;  (pack-record fields page rec offset))
  (doseq [rec (:records page)]
    (pack-record page fields rec (:offset (meta rec))))
  page)


(defn records->slots [page]
  (mapv (comp :offset meta) (:records page)))

(defn pack-page
  "writes header and slots to memory"
  [page]
  (let [header (gloss.io/encode header-frame page)
        slots (gloss.io/encode slots-frame (records->slots page)) ; (:slots page)
        buffer (:raw page)]
    (.rewind buffer)
    (.put buffer (byte-streams/to-byte-array header))
    (if-not (empty? (:slots page))
      (do (.position buffer (- (.capacity buffer) (gloss.core/byte-count slots)))
          (.put buffer (byte-streams/to-byte-array (gloss.io/contiguous slots)))
          (if-let [fields (:fields page)]
            (pack-records page fields))))
    (.rewind buffer)
    page))


(defn unpack-record
  [page fields offset]
  (let [raw (:raw page)]
    (.position raw offset)
    (with-meta
      (rec/unpack-record fields (byte-streams/to-byte-array raw))
      {:offset offset})))

(defn unpack-records
  [page fields]
  {:records (mapv (partial unpack-record page fields) (:slots page))})

(defn unpack-page
  [page]
  (let [raw (:raw page)
        _ (.rewind raw)
        header (gloss.io/decode header-frame raw false)
        slots-count (:slot-count header)
        slots (if (> slots-count 0)
                (do (.position raw (- config/*page-size* (* slots-count rec/size-of-number)))
                    (gloss.io/decode slots-frame (byte-streams/to-byte-array raw)))
                [])
        readed-page (conj page
                          header
                          {:slots slots})]
    (conj readed-page (if-let [fields (:fields page)] (unpack-records readed-page fields)))))

(defn write-page
  "flushes page to disk if it is modified"
  [page]
  (if (:dirty page)
    (f/write-page (pack-page page))
    page))

(defn read-page
  [page]
  (if (f/has-page page)
    (let [buffer (f/read-page page)
          p (make-page (assoc page :raw buffer))]
      (unpack-page p))))


(defn has-space-for-record?
  [page record]
  (let [fields (:fields page)
        space (- (:free-count page) (-> page :slot-count inc (* rec/size-of-number)))
        rec-bin (rec/pack-record fields record)
        rec-size (gloss.core/byte-count rec-bin)]
    (>= space rec-size)))


(defn can-insert-now?
  [page record]
  (let [fields (:fields page)
        space (- config/*page-size* (:free-offset page) (-> page :slot-count inc (* rec/size-of-number)))
        rec-bin (rec/pack-record fields record)
        rec-size (gloss.core/byte-count rec-bin)]
    (>= space rec-size)))


(defn records
  [page]
  (vec (:records page)))


(declare insert-record)


(defn insert-records
  [page records]
  (if (empty? records)
    page
    (recur (insert-record page (first records)) (rest records))))

(defn replace-records
  [page records]
  (let [new-page (assoc page :free-offset header-frame-size
                             :free-count (free-space {:slot-count 0})
                             :slots []
                             :records []
                             :slot-count 0
                             :dirty true)]
    (insert-records new-page records)))

(defn optimize-space
  [page]
  (replace-records page (records page)))

(defn insert-record
  [page record]
  (when-not (can-insert-now? page record) (optimize-space page))
  (let [fields (:fields page)
        free-offset (:free-offset page)
        slot-count (inc (:slot-count page))
        ;slots (conj (vec (:slots page)) free-offset)
        rec-bin (rec/pack-record fields record)
        rec-size (gloss.core/byte-count rec-bin)
        new-free-offset (+ free-offset rec-size)
        new-free (- (:free-count page) rec-size rec/size-of-number)
        rec (with-meta record {:offset free-offset})
        records (conj (vec (:records page)) rec)
        key (:key (meta fields))
        sorted-records (sort-by key compare records)
        new-page (assoc page :slot-count  slot-count
                             :records     (vec sorted-records)
                             :free-offset new-free-offset
                             :free-count  new-free)]
    (assoc new-page :dirty true
                    :slots (records->slots new-page))))

(defn split
  [page record]
  (let [records (:records page)
        key (:key (meta (:fields page)))
        median (int (/ (count records)
                       2))
        new-left (->> (subvec records 0 median) (into []))
        new-right (->> (subvec records median) (into []))
        new-page (-> (new-page {:type (:type page)
                                :fields (:fields page)})
                     (with-records (:fields page))
                     (insert-records new-right)
                     (assoc :prev-page (:page-id page)))
        page (-> page (replace-records new-left) (assoc :next-page (:page-id new-page)))
        new-pivot (key (first new-right))
        key-value (key record)
        key-gt-pivot (compare key-value new-pivot)]
    [(if (< key-gt-pivot 0) (insert-record page record) page)
     (if (>= key-gt-pivot 0) (insert-record new-page record) new-page )
     new-pivot]))

(defn ensure-records
  [page fields]
  (or (:records page)
      (let [new-page (->> page (with-records fields))
            records (unpack-records new-page fields)]
        (conj new-page records))))

(defn delete-record
  [page n]
  )

(defn find-record
  [page key-value]
  (let [records (records page)
        fields (:fields page)
        key-name (:key (meta fields))
        comparator (fn [a b] (> 0 (compare (key-name a) (key-name b))))
        ind (core/binary-search records {key-name key-value} comparator)]
    (when ind (records ind))))

(defn find-record-or-prev
  [page key-value]
  (let [records (records page)
        fields (:fields page)
        key-name (:key (meta fields))
        comparator (fn [a b] (> 0 (compare (key-name a) (key-name b))))
        ind (core/binary-search-or-prev records {key-name key-value} comparator)]
    (if (> ind -1) (records ind) (first records))))

(defn update-record
  [page record])
(ns dbms.page.data
  (:require [dbms.record :as rec]
            [dbms.page.core :as page]))

(defmethod rec/insert-record :data
  [page record]
  (if (page/has-space-for-record? page record)
    [(page/insert-record page record) nil nil]              ; [page new-leaf new-pivot]
    (page/split page record)))

(defmethod rec/find-record :data
  [page key-value]
  (page/find-record page key-value))

(ns dbms.record
  (:require [gloss core io]))

(def size-of-number 4)
(def field-int :int32)
(def field-varchar (gloss.core/finite-frame (gloss.core/prefix :int16) (gloss.core/string :utf-8)))
(def field-pid {:page-id field-int})

;(defmulti find-records (fn [obj & _] (:type obj)))
;(defmethod find-records :default [& _] '())
; page index-frame data-frame index-val

(def codecs {:number field-int
             :string field-varchar})

(defn type->codec [type]
  (codecs type))

(defn record-frame
  [fields]
  (let [codec (zipmap (keys fields) (map type->codec (vals fields)))]
    (gloss.core.structure/compile-frame codec)))

(defn pack-record
  [fields rec]
  (->> rec (gloss.io/encode (record-frame fields))))


(defn unpack-record
  [fields buffer]
  (gloss.io/decode (record-frame fields) buffer false))


(defmulti insert-record :type)
(defmulti find-record :type)
(ns dbms.file
  (:require [dbms.config :as config])
  (:import [java.io RandomAccessFile]
           (java.nio ByteBuffer))
  (:use [clojure.java.io]))


(defn file-name [] (:file-name config/*conn*))

(defn page-count []
  (let [fname (file-name)
        flength (.length (file fname))]
    (/ flength config/*page-size*)))

(defn has-page [pid]
  (let [cnt (page-count)
        index (:page-id pid)]
    (<= index cnt)))

(defn get-raf []
  (RandomAccessFile. (file-name) "rw"))

(defn read-page [page]
  (with-open [raf (get-raf)]
    (let [offset (* config/*page-size* (:page-id page))
          buffer (byte-array config/*page-size*)]
      (.seek raf offset)
      (.read raf buffer)
      (ByteBuffer/wrap buffer))))

(defn write-page [page]
  (with-open [raf (get-raf)]
    (let [offset (* config/*page-size* (:page-id page))
          buffer (:raw page)]
      (.seek raf offset)
      (.write raf (byte-streams/to-byte-array buffer))))
  page)




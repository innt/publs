(ns dbms.core)

(defn pid
  [page]
  {:page-id (:page-id page)})

(defn binary-search [v target f]
  (loop [low 0
         high (dec (count v))]
    (if (> low high)
      false
      (let [mid (quot (+ low high) 2)
            mid-val (v mid)]
        (cond (f mid-val target) (recur (inc mid) high)
              (f target mid-val) (recur low (dec mid))
              :else mid)))) )


(defn binary-search-or-prev [v target f]
  (loop [low 0
         high (dec (count v))]
    (if (> low high)
      high
      (let [mid (quot (+ low high) 2)
            mid-val (v mid)]
        (cond (f mid-val target) (recur (inc mid) high)
              (f target mid-val) (recur low (dec mid))
              :else mid)))) )

;(defn binary-search-2 [v target f]
;  (loop [low 0
;         high (dec (count v))]
;    (if (> low high)
;      (- (inc low))
;      (let [mid (quot (+ low high) 2)
;            mid-val (v mid)]
;        (cond (f mid-val target) (recur (inc mid) high)
;              (f target mid-val) (recur low (dec mid))
;              :else mid)))) )

(defn records
  "returns lazy sequence of records on page, (sorted by PK or KEY)

  it is reversable (rseq (records page))

  (count (records page)) -> does not realize seq, count is cached

  it has IFn interface
  (def recs (records page))
  (recs 2) - gives you second element {:id 12 :name :sam}

  (binary-search (records page) 100 (fn [r1 r2] (> (:id r1) (:id r2) ) ) )
  "
  [page]
  )
(ns dbms.cache
  (:require [clojure.core.cache :refer :all]
            [dbms.page.core :as page]
            [taoensso.timbre :as timbre]))

(defcache CustomCache [cache on-evict]
          CacheProtocol
          (evict [this key]
                 (let [v (get cache key ::miss)]
                   (if (= v ::miss)
                     this
                     (do
                       (on-evict v)
                       (CustomCache. (dissoc cache key) on-evict)))))
          (miss [this item result] (CustomCache. (assoc cache item result) on-evict))
          (lookup [_ item] (get cache item))
          (lookup [_ item not-found] (get cache item not-found))
          (has? [_ item] (contains? cache item))
          (hit [this item] this)
          (seed [_ base] (CustomCache. base on-evict))
          Object
          (toString [_] (str cache)))


(defn write-if-dirty-factory
  [base on-evict]
  {:pre [(map? base)]}
  (CustomCache. base on-evict))


(def ^:dynamic *pages* (atom (-> {}
                                 (write-if-dirty-factory page/write-page)
                                 (lru-cache-factory :threshold dbms.config/*page-cache-limit*))))

(defn pull-page [page]
  (lookup
    (if (has? @*pages* page)
      (swap! *pages* #(hit % page))
      (swap! *pages* #(miss % page (page/read-page page))))
    page))


(defn push-page [page]
  (swap! *pages* #(miss % {:page-id (:page-id page)} page))
  page)



(defn clear []
  (doseq [pid (keys @*pages*)]
    (swap! *pages* #(evict % pid)))
  @*pages*)


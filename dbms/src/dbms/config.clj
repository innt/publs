(ns dbms.config)

(def ^:dynamic *page-size* 100)                             ; phisical size of page in bytes 8192
(def ^:dynamic *page-cache-limit* 2)                        ; how much pages to store in memory
(def ^:dynamic *conn* {:file-name "data.db"})

(ns dbms.table
  (:require
    [dbms.record :as rec]
    [dbms.page.core :as page]
    [dbms.cache :as cache]
    [gloss core])
  (:import [java.nio ByteBuffer]))

;(comment
;  (def students {:file-id      0
;                 :root-page    {:page-id 1, :file-id 0}
;                 :record-frame (gloss.core.structure/compile-frame {:address rec/field-varchar
;                                                                    :email   rec/field-varchar
;                                                                    :id      rec/field-int
;                                                                    :name    rec/field-varchar})
;                 :index-frame  (gloss.core.structure/compile-frame {:id rec/field-int, :pid rec/field-pid})
;                 :pk           :id})
;
;  (def employee {:file-id      1
;                 :root-page    {:page-id 1, :file-id 1}
;                 :record-frame (gloss.core.structure/compile-frame {:id          rec/field-int
;                                                                    :name        rec/field-varchar
;                                                                    :designation rec/field-varchar
;                                                                    :address     rec/field-varchar})
;                 :index-frame  (gloss.core.structure/compile-frame {:id rec/field-int, :pid rec/field-pid})
;                 :pk           :id})
;
;  (defn create-table
;    [table]
;    (let [page (page/new-page (:file-id table))]
;      (page/write-page (conj page {:type :index, :page-id 1}))))
;
;  (defn find-page
;    [table pk]
;    (cache/get-page (:root-page table)))
;
;  (defn find-record
;    [table pk page]
;    (let [page (find-page table pk)
;          slots (:slots page)]
;      ))
;
;  (defn delete
;    [table pk]
;    )
;
;  (defn insert
;    [table record]
;    (let [page (find-page table record)
;          offset (:free-offset page)
;          record-raw (gloss.io/encode (:record-frame table) record)
;          buffer (:raw page)
;          free-count (- (:free-count page) (gloss.core/byte-count record-raw) rec/field-int-size)
;          free-offset (+ (:free-offset page) (gloss.core/byte-count record-raw))]
;      ; we are on needed page, we have enough space
;      (.position buffer offset)
;      (.put buffer (gloss.io/contiguous record-raw))
;      (cache/update-page (conj page {:free-count  free-count
;                                     :free-offset free-offset
;                                     :slot-count  (inc (:slot-count page))
;                                     :slots       (conj (:slots page) offset)}))))
;  )
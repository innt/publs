(ns dbms.init
  (:require [dbms.config :as config]
            [dbms.cache :as cache]
            [dbms.table :as table]
            [dbms.page.core :as page])
  (:use [clojure.java.io]))

(defn init-file
  []
  (let [page (assoc (page/new-page) :type :boot)]
    (page/write-page page)))

;(defn init-files []
;  (doseq [[ind name] config/file-list]
;    (if (.exists (as-file name)) (delete-file (as-file name)))
;    (init-file ind)))
;
;(defn create-tables []
;  (table/create-table table/students)
;  (table/create-table table/employee))
;
;(defn init []
;  (init-files)
;  (create-tables)
;  true)
;
;(defn stop []
;  (cache/flush-cache))

(defn stop []
  (cache/clear))
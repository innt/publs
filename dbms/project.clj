(defproject dbms "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [gloss "0.2.5"]
                 [org.clojure/core.cache "0.6.4"]
                 [com.taoensso/timbre "4.1.4"]]
  :main ^:skip-aot dbms.main
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})

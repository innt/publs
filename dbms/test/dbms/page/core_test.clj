(ns dbms.page.core-test
  (:require [clojure.test :refer :all]
            [dbms.page.core :as page]
            [dbms.config :as config]
            [dbms.record :as rec]
            [dbms.cache :as cache])
  (:import (java.nio ByteBuffer)
           (java.io File)))

(deftest pack-unpack
  (testing "Pack page then unpack, check records are ordered by key"
    (let [p1 (-> {:type :data :page-id 1}
                 (page/new-page)
                 (page/with-records (with-meta {:id :number :name :string} {:key :id}))
                 (page/insert-record {:id 5 :name "John"})
                 (page/insert-record {:id 3 :name "Smith"})
                 (page/pack-page))
          buf (.duplicate ^ByteBuffer (:raw p1))
          p2 (-> (page/new-page)
                 (page/with-records (with-meta {:id :number :name :string} {:key :id}))
                 (assoc :raw buf)
                 (page/unpack-page))]
      (is (= [{:id 3 :name "Smith"}
              {:id 5 :name "John"}]
             (page/records p2)))
      (is (= 1 (:page-id p2)))
      (is (= :data (:type p2))))))


(deftest read-write-file
  (testing "write page in file, then read it"
    (let [temp-file-name "filename"
          temp-file (File/createTempFile temp-file-name ".txt")
          p1 (-> {:type :data :page-id 1 :dirty true}
                 (page/new-page)
                 (page/with-records (with-meta {:id :number :name :string} {:key :id}))
                 (page/insert-record {:id 5 :name "John"})
                 (page/insert-record {:id 3 :name "Smith"}))]
      (with-bindings {#'config/*conn* {:file-name (str temp-file-name ".txt")} }
        (page/write-page p1)
        (let [p2 (-> {:page-id 1}
                     (page/with-records (with-meta {:id :number :name :string} {:key :id}))
                     (page/read-page))]
          (is (= [{:id 3 :name "Smith"}
                  {:id 5 :name "John"}]
                 (page/records p2)))
          (is (= 1 (:page-id p2)))
          (is (= :data (:type p2))))
        (clojure.java.io/delete-file (str temp-file-name ".txt"))))))


(deftest split-page
  (testing "splits data page"
    (let [page (-> {:type :data :page-id 100}
                   (page/new-page)
                   (page/with-records (with-meta {:id :string :value :string} {:key :id}))
                   (page/insert-record {:id "a2" :value "val2"})
                   (page/insert-record {:id "a5" :value "val5"}))
          [new-page new-leaf new-pivot] (page/split page {:id "a3" :value "val3"})]
      (is (= [{:id "a2" :value "val2"} {:id "a3" :value "val3"}] (page/records new-page)))
      (is (= [{:id "a5" :value "val5"}] (page/records new-leaf)))
      (is (= new-pivot "a5"))
      (is (= :data (:type page)))
      (is (= -1 (:next-page page)))
      (is (= 100 (:prev-page new-leaf))))))


(deftest page-replace-records
  (testing "replace records"
    (let [page (-> {:type :data :page-id 0}
                   (page/new-page)
                   (page/with-records (with-meta {:id :string :value :string} {:key :id}))
                   (page/insert-record {:id "a2" :value "val2"})
                   (page/insert-record {:id "a5" :value "val5"}))
          new-page (page/replace-records page [{:id "a10" :value "val10"}])]
      (is (= [{:id "a10" :value "val10"}] (page/records new-page))))))


(comment
  (with-bindings {#'config/*conn* {:file-name "temp.txt"}
                  #'config/*page-size* (+ page/header-frame-size 16 8)
                  #'cache/*pages* (atom (-> {}
                                            (cache/write-if-dirty-factory page/write-page)
                                            (clojure.core.cache/lru-cache-factory :threshold 100)))}
    (let [temp-file (File/createTempFile "temp" ".txt")
          flds (with-meta {:id :number :value :number} {:key :id})
          page (-> {:type :data :page-id 0}
                   (page/new-page)
                   (page/with-records flds)
                   (page/insert-record {:id 4 :value 40})
                   (page/insert-record {:id 9 :value 90}))]
      (cache/push-page page)
      (let [[page new-page new-pivot] (rec/insert-record page {:id 8 :value 80})]
        (cache/push-page page)
        (cache/push-page new-page))
      (clojure.java.io/delete-file "temp.txt")
      cache/*pages*)))